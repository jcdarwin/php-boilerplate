<?php

namespace src;

// the suffix used as a suffix to the SOAP service route to serve the WSDL.
define('WSDL_ROUTE', '/wsdl');

// the absolute path to our views directory.
define('TEMPLATE_PATH', __DIR__.'/templates');

// the absolute path to our lib directory.
define('LIB_PATH', __DIR__.'/lib');

// the suffix for our templates
define('TEMPLATE_SUFFIX', '.tpl');

// do we want to allowing caching of the WSDL (1) or not (0)?
define('WSDL_CACHE_ENABLED', '0');

// do we want to print debug messages?
define ('DEBUG', false);
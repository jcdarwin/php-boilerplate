<?php include TEMPLATE_PATH . '/includes/header.tpl'; ?>

        <div class="hero-unit">

            <div class="cat home">
            </div>

            <h1>PHP boilerplate</h1>
            <p>A minimal boilerplate PHP web-app, providing the best-practice architecture of a modern PHP project (PHP5.4+) within a small footprint. This project also demonstrates:</p>

            <ul>
                <li><a href="<?php echo 'connector' . WSDL_ROUTE; ?>">A SOAP server</a></li>
                <li><a href="<?php echo 'client/add?service=connector&x=10&y=5'; ?>">A SOAP client</a></li>
            </ul>

            <div class="well">
                <h2>SOAP service to add two numbers:</h2>
                <form action="client/add" method="GET">
                    <label>X:</label>
                    <input type="text" name="x">
                    <label>Y:</label>
                    <input type="text" name="y">
                    <input type="hidden" name="service" value="connector">
                    <input type="submit" value="Add">
                </form>
            </div>

            <p>This project should provide a reasonable starting point for building a typical PHP web application.</p>

            <p>Out of the box we provide:</p>
            <ul>
                <li>a single front-end controller</li>
                <li>controllers, views and templates</li>
                <li>a router allow a given route to be associated with either a closure or a class method</li>
            </ul>

            <p>We use tools to make development tasks easier, including:</p>
            <ul>
                <li><a href="https://getcomposer.org/">Composer</a> for dependency management</li>
                <li><a href="http://yeoman.io/">Yeoman</a> for scaffolding our web app</li>
                <li><a href="http://gruntjs.com/">Grunt</a> for running development tasks</li>
                <li><a href="http://phpunit.de/">PHPUnit</a> for unit testing</li>
            </ul>

            <p>Some ideas / conventions are cribbed from <a href="http://laravel.com/">Laravel</a>, although this framework is very minimal/lightweight in comparison &#8212; should you need something more fully-featured than the functionality offered here, <a href="http://laravel.com/">Laravel</a> would be a good choice.</p>

            <p>Refer to the <a href="/readme">README file</a> for more details</p>

            <p class="small"><em>Cats courtesy of <a href="http://dribbble.com/iconka/projects/151578-Cat-Power">Denis Sazhin / Iconka</a></em></p>
        </div>

<?php include TEMPLATE_PATH . '/includes/footer.tpl'; ?>
    
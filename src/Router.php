<?php

namespace src;
use src\traits as traits;

class Router {

    use traits\Log;

    public $matched;

    public function __construct() {
    }

    /**
     * Our method for routing GET requests.
     *
     * @param string  $route
     * @param mixed   $action     : either a closure or a string indicating the controller@method
     * @param mixed   $parameters : optional - either an array or a closure that will produce an array
     * @param mixed   $render     : optional - either a closure or a string indicating the view class.
     */
    public function get($route, $action, $parameters = array(), $render = '') {

        if ( !count($_POST) ) {
            // Strip off the query string before matching the route.
            $uri = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);

            // As a route will typically include '\', use a different delimiter in the regexp match.
            if (preg_match('{^' . $route . '$}', $uri)) {

                $this->matched = true;

                // We may have been passed a closure to determine our parameters.
                if (is_callable($parameters)) {
                    $parameters = call_user_func($parameters);
                }
                $this->route($route, $action, $parameters, $render);
            }
        }
    }

    /**
     * Our method for routing POST requests.
     *
     * @param string  $route
     * @param mixed   $action     : either a closure or a string indicating the controller@method
     * @param mixed   $parameters : optional - either an array or a closure that will produce an array
     * @param mixed   $render     : optional - either a closure or a string indicating the view class.
     */
    public function post($route, $action, $parameters = array(), $render = '') {

        if ( isset($_POST['_method']) && strtoupper($_POST['_method']) == 'POST') {
            // Strip off the query string before matching the route.
            $request_url = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);

            // As a route will typically include '\', use a different delimiter in the regexp match.
            if (preg_match('{^' . $route . '$}', $request_url)) {

                $this->matched = true;

                // We may have been passed a closure to determine our parameters.
                if (is_callable($parameters)) {
                    $parameters = call_user_func($parameters);
                }

                $service = isset($_POST['service']) ? $_POST['service'] : '';
                $this->route($route, $action, $parameters, $render);
            }
        }
    }

    /**
     * Action the route request.
     *
     * @param string  $route
     * @param mixed   $action     : either a closure or a string indicating the controller@method
     * @param mixed   $parameters : optional - either an array or a closure that will produce an array
     * @param mixed   $render     : optional - either a closure to render the results, or an empty string.
     */
    protected function route($route, $action, $parameters, $render = '') {

        if ($this->routingToController($action)) {
            // We're routing to a controller.

            // Controller actions are in the form 'ControllerClassName@methodName'.
            list($class, $method) = explode('@', $action);

            // Determine any arguments to pass to the controller.
            $constructorArgs = array();
            if ( isset($_GET['service']) ) {
                $constructorArgs = array($_GET['service']);
            }

            // Instantiate the controller class
            $instance = $this->build($class, $constructorArgs);

            try {
                // Call the method on the instance
                $result = $this->callAction($instance, $method, $parameters);

                // Render any results
                if (is_callable($render)) {
                    // We've got a closure
                    call_user_func($render, $result);
                } elseif ($render) {
                    // We've got the name of a view class which extends View.php.
                    // Instantiate the view class.
                    $view = $this->build($render, array());
                    $view->render();
                } else {
                    // We've not been provided with a view - simply spit out any results.
                    echo $result;
                }
            } catch (Exception $e) {

                if (LOG) {
                    $this->log(__FUNCTION__.": ERROR: " . $e->getMessage() . "\n");
                }

                echo ( __FUNCTION__.": ERROR: " . $e->getMessage() . "\n");
            }

        } else {
            // We're routing to a closure.
            $action();
        }
    }

    /**
     * Determine if the action is routing to a controller.
     *
     * @param  mixed  $action
     * @return bool
     */
    protected function routingToController($action) {
        if ($action instanceof Closure) return false;

        return is_string($action);
    }

    /**
     * Instantiate a concrete instance of the given type.
     *
     * @param  string  $concrete
     * @param  array   $parameters
     * @return mixed
     */
    protected function build($concrete, $constructorArgs) {
        $reflector = new \ReflectionClass($concrete);
        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return new $concrete();
        }

        return $reflector->newInstanceArgs($constructorArgs);
    }

    protected function callAction($instance, $method, $parameters) {
        return call_user_func_array(array($instance, $method), $parameters);
    }
}
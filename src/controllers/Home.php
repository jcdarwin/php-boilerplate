<?php

namespace src\controllers;
use src\views as views;

/**
 * Our controller for the home page.
 * Note that the controller also acts as a view, extending 
 * from the View class.
 */
class Home extends views\View {

    public function __construct() {
        $reflector = new \ReflectionClass(__CLASS__);
        $template = strtolower( $reflector->getShortName() . TEMPLATE_SUFFIX );
        parent::__construct( '', $template );
    }

}
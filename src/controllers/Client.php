<?php

namespace src\controllers;

class Client extends SoapClientBase {

    protected $soapService;

    /**
     * Create our instance, obtaining a handle to the 
     * SOAP service from our parent.
     */
    public function __construct($service) {
        $soapService = parent::__construct($service);
    }

    /**
     * Our add method
     */
    public function add ($x, $y) {

        // Call the method on our SOAP service.
        $result = $this->soapService->add($x, $y);

        // Debug messages if so configured.
        if (DEBUG) {
            $this->debug();
        }

        return $result;
    }
}
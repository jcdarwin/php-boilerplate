<?php

namespace src\controllers;
use src\interfaces as interfaces;

/**
 * Our SOAP client base class
 */
class SoapClientBase implements interfaces\SoapClient {

    private $soap;
    private $result;
    protected $soapService;

    /**
     * Create our instance, using the SOAP helper to provide
     * a handle to the provided service.
     */
    public function __construct($service) {

        // Instantiate our SOAP helper.
        require_once LIB_PATH.'/Soap.php';
        $this->soap = new \Soap();

        // Connect to our SOAP service.
        $this->soapService = $this->soap->connect("http://$_SERVER[HTTP_HOST]", $service);

        $result = $this->soapService;
        return $result;
    }

    protected function debug() {
        echo("\nReturning value of __soapCall() call: ".$this->result);

        echo("\nDumping request headers:\n" 
          .$this->soapService->__getLastRequestHeaders());


        echo("\nDumping request:\n".$this->soapService->__getLastRequest());

        echo("\nDumping response headers:\n"
          .$this->soapService->__getLastResponseHeaders());

        echo("\nDumping response:\n".$this->soapService->__getLastResponse());
    }

}
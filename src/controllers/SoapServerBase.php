<?php

namespace src\controllers;
use src\interfaces as interfaces;

/**
 * Our SOAP server base class
 */
class SoapServerBase implements interfaces\SoapServer {

    private $soap;

    public function __construct() {
        require_once LIB_PATH.'/Soap.php';
        $this->soap = new \Soap();
    }

    /**
     * Generate the WSDL for the child class.
     *
     * @return String
     */
    public function wsdl($class) {
        // ensure the XML gets served with the correct mimetype
        header('Content-Type: application/xml; charset=utf-8');
        return $this->soap->generateWSDL( $class, $this->route() );
    }

    /**
     * Serve the public methods on the child class as a SOAP service.
     */
    public function serve($class) {
        return $this->soap->serve( $this, $class, $this->route_wsdl() );
    }

    /**
     * Return the route as the name of the child class,
     * stripped of any namespace and lowercased,
     * and appended with the WSDL suffix.
     *
     * @return string
     */
    private function route_wsdl() {
        return $this->route() . WSDL_ROUTE;
    }

    /**
     * Return the route as the name of the child class,
     * stripped of any namespace and lowercased.
     *
     * @return string
     */
    private function route() {
        return strtolower( self::getShortname(get_class($this)) );
    }

    private function getShortname($class) {
        return substr( $class, strrpos($class, '\\') + 1 );
    }

}
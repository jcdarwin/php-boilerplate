<?php

namespace src\controllers;

/**
 * Our SOAP service class
 *
 */
class Connector extends SoapServerBase {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Generate the WSDL for this class
     *
     * We must override the method of our parent class, and
     * pass in the name of our class.
     *
     * @return String
     */
    public function wsdl($class = __CLASS__) {
        echo parent::wsdl($class);
    }

    /**
     * Serve the public methods on this class as SOAP methods.
     *
     * We must override the method of our parent class, and
     * pass in the name of our class.
     *
     * @return String
     */
    public function serve($class = __CLASS__) {
        echo parent::serve($class);
    }

    /**
     * Add method.
     *
     * We decorate with the params and return value in order that
     * the information will appear in the WSDL.
     *
     * @param Int $x
     * @param Int $y
     * @return Int
     */
    public function add($x, $y) {
        return $x + $y;
    }

}
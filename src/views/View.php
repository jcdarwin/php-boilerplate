<?php

namespace src\views;
use src\interfaces as interfaces;

class View implements interfaces\View {

    private $template;
    private $data;
    protected $base;

    /*
    ** We can specify a template, or fallback to a template that has 
    ** the same name as the child class.
    */
    public function __construct($data = '', $template = '') {
        if ($data) {
            $this->data = $data;
        }

        // We want all our assets (js / css etc) to be pulled in relative to the web root,
        // no matter how many steps in the route we're serving.
        $this->base = "http://$_SERVER[HTTP_HOST]";

        if ($template) {
            $this->template = $template;
        } else {
            $this->template = strtolower( self::getShortname(get_class($this)) ) . TEMPLATE_SUFFIX;
        }
    }

    private function getShortname($class) {
        return substr( $class, strrpos($class, '\\') + 1 );
    }

    public static function viewPath () {
        return "";
    }

    public function setTemplate ($template = '') {
        $this->template = $template;
    }

    public function populate ($data = '') {
        $this->data = $data;
    }

    /*
    ** If no template exists, simply spit out our data.
    */
    public function render () {
        $template = TEMPLATE_PATH . '/' . $this->template;

        if (file_exists($template) && is_file($template)) {
            include($template);
        } else {
            echo $this->data;
        }
    }

    public function sanitise($string, $pattern) {

    }
}
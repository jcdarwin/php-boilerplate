<?php

namespace src\views;

trait Sanitise {
    public function sanitise($string, $pattern) {
        return preg_replace($pattern, '', $string);
    }

    public static $sanitiseInt = '/^[^1-9][^0-9]*$/';
}

class Client extends View {
    use Sanitise;

    private $x;
    private $y;

    public function __construct($data = '', $template = '') {
       parent::__construct($data, $template);
    }

    public function setX($x) {
        $this->x = $x;
    }

    public function getX() {
        return $this->x;
    }

    public function setY($y) {
        $this->y = $y;
    }

    public function getY() {
        return $this->y;
    }
}
<?php

/**
 * A wrapper for Zend's SOAP helper libraries.
 * This class is not namespaced, as this appears to cause problems
 * with Zend's autoloader.
 */
class Soap {

    protected $soapService;

    public function __construct() {
    }

    /**
     * Generate WSDL for the supplied class name.
     *
     * @param string $class: the namespaced classname we're generating WSDL for.
     * @param string $route: the route for the service described by the WSDL.
     */
    public function generateWSDL($class, $route) {
        $autodiscover = new Zend\Soap\AutoDiscover();
        $autodiscover->setClass($class)
                     ->setUri("http://$_SERVER[HTTP_HOST]/$route");
//                    ->setServiceName('MySoapService');
        //$wsdl = $autodiscover->generate();
        return $autodiscover->toXml();
    }

    /**
     * Serve the supplied class instance as a SOAP service, with the service being 
     * described using the supplied route for the WSDL.
     * 
     * @param Base $instance: an instance of the Base class
     * @param string $class: the namespaced class we're serving as a SOAP service.
     * @param string $route_wsdl: the route for the WSDL describing this service.
     */
    public function serve($instance, $class, $route_wsdl) {
        $server = new Zend\Soap\Server("http://$_SERVER[HTTP_HOST]/" . $route_wsdl);
        $server->setClass($class);
        $server->setObject($instance);
        try {
            $server->handle();
        }
        catch (Exception $e) {
            $server->fault('Sender', $e->getMessage());
        }
    }

    /**
     * Connect to the SOAP service exposed at the specified uri / class.
     *
     * @param string $uri: the uri of the SOAP service we want to connect to.
     * @param string $class: the name of the class on the SOAP service whose methods we wish to use.
     * @param boolean $debug: whether we wish to print debug information about the SOAP request.
     * @return SOAPClient
     */
    public function connect($uri, $class) {
        // Set WSDL caching according to config/
        ini_set("soap.wsdl_cache_enabled", WSDL_CACHE_ENABLED);

        $options = array(
            'uri'      => "$uri/",
            'location' => "$uri/$class",
            'trace'    => 1
        );

        $client = new SOAPClient(null, $options);

        return $client;
    }
}

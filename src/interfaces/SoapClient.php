<?php
namespace src\interfaces;

/**
 * The interface for any SOAP client classes.
 */
interface SoapClient {
 
    public function __construct($service);

}
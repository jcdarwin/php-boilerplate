<?php
namespace src\interfaces;

/**
 * The interface for any view classes.
 */
interface View {
 
    public function __construct();

    public function render();

    public function sanitise($string, $pattern);

}
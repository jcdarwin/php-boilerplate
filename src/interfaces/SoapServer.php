<?php
namespace src\interfaces;

/**
 * The interface for any SOAP server classes.
 */
interface SoapServer {
 
    public function __construct();

    public function wsdl($class);

    public function serve($class);
}

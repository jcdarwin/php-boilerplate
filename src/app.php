<?php

namespace src;
use src\traits as traits;

/**
 * We rely on composer's autoloader.
 * To include classes in new directories, add the path to the 'autoload'
 * parameter in the composer.json and run 'composer dump-autoload'.
 */
require __DIR__.'/../vendor/autoload.php';

/**
 * Pull in our configuration.
 */
require_once __DIR__.'/config.php';

/**
 * The class that controls our app.
 */
class app {

    use traits\Log;

    /**
     * The function that runs our app.
     * Routes are defined here, along with the corresponding action,
     * the paramters to pass to the action, and the rendering function.
     */
    public function run() {

        /**
         * Instantiate our router.
         * The signature for the methods provided by our router is:
         *
         * @param string   $route
         * @param mixed    $action     : either a closure or a string indicating the controller@method
         * @param mixed    $parameters : either an array or a closure that will produce an array
         * @param \Closure $render
         */
        $router = new Router();

        /**
         * Our home page.
         *
         * The most compact form of routing.
         * We have a route matching a path, and we supply a closure that 
         * performs the action and also renders the result.
         * Note that the Home controller is also acting as the view.
         */
        $router->get('/', function(){
            $class = new controllers\Home();
            $class->render();
        });

        $router->get('/readme', function(){
            include '../README.md';
        });

        /**
         * Our SOAP service.
         *
         * Similar to the previous route, but we don't even instantiate
         * a view, instead simply echoing the results returned from the controller.
         */
        $router->get('/connector', function(){
            $class = new controllers\Connector();
            echo $class->serve();
        });

        /**
         * The WSDL for our SOAP service.
         *
         * Similar to the previous route, but we don't even instantiate
         * a view, instead simply echoing the results returned from the controller.
         */
        $router->get('/connector'.WSDL_ROUTE, function(){
            $class = new controllers\Connector();
            echo $class->wsdl();
        });

        /**
         * Our SOAP client calling a method on the SOAP service.
         * E.G. http://localhost.soap/client/add?service=connector&x=10&y=5
         *
         * A more complex route, where we have a route matching a path, 
         * an action specified in the form of a controller and method to run,
         * and closures for the parameters and the renderer.
         * Note that we're passing our parameters here in the form of a closure,
         * as allows us to pass custom logic that the router can use to determine the parameters.
         * Note that we can also supply a view string, such as 'src\views\Client', rather than a closure.
         */
        $router->get('/client/add', 'src\controllers\Client@add',
            function() {
                $x = isset($_GET['x']) ? $_GET['x'] : '';
                $y = isset($_GET['y']) ? $_GET['y'] : '';
                return array($x, $y);
            },
            function($data) {
                $view = new views\Client($data);
                $view->setX( isset($_GET['x']) ? $_GET['x'] : '' );
                $view->setY( isset($_GET['y']) ? $_GET['y'] : '' );
                $view->render();
            }
        );

        /**
         * An other form of the above route, using an closure for the action
         * which performs the logic and the rendering.
         */
        /*
        $router->get('/client/add', function(){
            if (isset($_GET['x']) && isset($_GET['y'])) {
                $class = new controllers\Client('connector');

                // can also do: $view = new views\Client($data, $template);
                $view = new views\Client();

                $view->populate(
                    $class->add(
                        $view->sanitise($_GET['x'], $view::$sanitiseInt),
                        $view->sanitise($_GET['y'], $view::$sanitiseInt)
                    )
                );

                $view->render();
            }
        });
        */
        
        if (!$router->matched) {
            $router->get('/404.html', function(){
                include '../app/404.html';
            });
        }

    }
}
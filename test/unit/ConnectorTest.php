<?php

namespace src;

/**
 * We rely on composer's autoloader.
 * To include classes in new directories, add the path to the 'autoload'
 * parameter in the composer.json and run 'composer dump-autoload'.
 */
require __DIR__.'/../../vendor/autoload.php';

/**
 * Pull in our configuration.
 */
require_once __DIR__.'/../../src/config.php';

class ConnectorTest extends \PHPUnit_Framework_TestCase {
    public function testConnector() {
        $class = new controllers\Connector();

        $this->assertEquals($class->add(2, 3), 5);
    }
}

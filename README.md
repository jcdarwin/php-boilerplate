<!DOCTYPE html>
<html>
<title>PHP boilerplate</title>

<xmp theme="united" style="display:none;">

# Description

A minimal boilerplate PHP web-app, providing the best-practice architecture of a modern PHP project (PHP5.4+) within a small footprint. This project also demonstrates a simple SOAP server and SOAP client implemented in PHP.

We provide:

* a single front-end controller
* controllers, views and templates
* a router allowing a given route to be associated with either a closure or a class method.
* unit tests

We use tools to make development tasks easier, such as:

* Composer
* Yeoman
* Grunt
* PHPUnit

This project should provide a reasonable starting point for building a typical PHP web application.

Some ideas / conventions are cribbed from [Laravel](http://laravel.com/), although this framework is very minimal/lightweight in comparison -- should you need something more fully-featured than the functionality offered here, [Laravel](http://laravel.com/) would be a good choice.

# Installation

* Clone this repo

* Ensure you have <a href="http://nodejs.org/">Node</a> (and therefore npm) installed.

* Install our node and bower dependencies using `npm`:

        npm install

* Install our PHP dependencies using `composer`:

        composer install

* Serve our project:

        grunt serve.php

Note although `grunt serve.php` works (using the PHP server provided by grunt-php), you might find it quicker to install this under MAMP / WAMP / XAMMP etc.

* Ensure that the webserver has permissions to write to `src/log`

* Visit the home page:

        http://127.0.0.1:8000/

# Architecture notes

This project is an example of a modern PHP application, in that it uses the following:

## a single front-end controller

Only one PHP file, `index.php`, is exposed in our webroot `app` directory (i.e. the only directory that should be directly readable by the webserver).

All other PHP files are included in the `src` directory, which lives outside of the webroot.

The `.htaccess` file (most of which is provided by the [Yeoman Webapp generator](https://github.com/yeoman/generator-webapp)) includes a number of useful sections to tighten security and aid performance, including:

* ensuring that non-web files are not served:

        <FilesMatch "(^#.*#|\.(bak|config|dist|fla|in[ci]|log|psd|sh|sql|sw[op])|~)$">

            # Apache < 2.3
            <IfModule !mod_authz_core.c>
                Order allow,deny
                Deny from all
                Satisfy All
            </IfModule>

            # Apache ≥ 2.3
            <IfModule mod_authz_core.c>
                Require all denied
            </IfModule>

        </FilesMatch>

* ensuring proper mimetypes

* defaulting to UTF-8 encoding for all text-based files

        # Use UTF-8 encoding for anything served as `text/html` or `text/plain`.
        AddDefaultCharset utf-8

        # Force UTF-8 for certain file formats.
        <IfModule mod_mime.c>
            AddCharset utf-8 .atom .css .js .json .jsonld .rss .vtt .webapp .xml
        </IfModule>

* blocking access to directories without a default document

        <IfModule mod_autoindex.c>
            Options -Indexes
        </IfModule>

* blocking access to hidden files and directories.

        <IfModule mod_rewrite.c>
            RewriteCond %{SCRIPT_FILENAME} -d [OR]
            RewriteCond %{SCRIPT_FILENAME} -f
            RewriteRule "(^|/)\." - [F]
        </IfModule>

* using gzip compression where possible

Do spend time reading through the `.htaccess` file, as you'll find plenty of useful configuration options to help with the security and performance of your project. Ideally, of course, these should be included in your main Apache conf, as the `.htaccess` itself has to be parsed on each request by Apache.

Importantly, we ensure that all non-asset requests are directed to our `index.php` front-end controller:

        # ##############################################################################
        # # REDIRECT ALL NON-ASSET REQUESTS TO index.php                               #
        # ##############################################################################

        <IfModule mod_rewrite.c>
            <IfModule mod_negotiation.c>
                Options -MultiViews
            </IfModule>

            RewriteEngine On

            # Redirect Trailing Slashes...
            RewriteRule ^(.*)/$ /$1 [L,R=301]

            # Handle Front Controller...
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^ index.php [L]
        </IfModule>

## the index.php file

Our `index.php` file is actually quite simple, as its only purpose is to call our main `app` class:

        namespace src;

        /**
         * Bootstrap our app.
         */
        require_once __DIR__.'/../src/app.php';

        /**
         * Instantiate our app.
         */
        $app = new app();

        /**
         * Run app.
         */
        $app->run();

## the app.php file

Our convention is to only define one class per file, with the name of the class (e.g. `app`) matching the name of the file (e.g. `app.php`).

Our `app.php` file is our main application controller, and does three things for us:

1. Include composer's autoloader:

        /**
         * We rely on composer's autoloader.
         * To include classes in new directories, add the path to the 'autoload'
         * parameter in the composer.json and run 'composer dump-autoload'.
         */
        require __DIR__.'/../vendor/autoload.php';

1. Include our configuration variables:

        /**
         * Pull in our configuration.
         */
        require_once __DIR__.'/config.php';

1. Define our routes, e.g.:

        $router->get('/readme', function(){
            include '../README.md';
        });

## Composer's autoloader

Composer's autoloader means that we don't have to write our own autoloader or require classes (with a couple of exceptions).

To use it, simply add the folder paths containing the classes to autoload to the `autoload` section of the `composer.json` file:

    "autoload": {
        "classmap": [
            "/",
            "src",
            "src/controllers",
            "src/views",
            "src/interfaces"
        ]
    }

If doing a `composer install`, the `vendor/autoload.php` file will be created for you, however if you subsequently add paths to the `autoload` section, you'll need to regenerate it:

    composer dump-autoload

## Composer dependency management

We use Composer to pull in PHP depedencies (namely, PHPUnit and Zend's SOAP helper):

    "require": {
        "zendframework/zend-soap": "2.0.*"
    },
    "require-dev": {
        "phpunit/phpunit": "4.0.*"
    },
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.zendframework.com/"
        }

## Routing and closures (PHP5.3+)

We provide our own `Router` class which allows us to associate an action with a given path. This action can either be a closure, or it can be a reference to a method on another class.

    /**
     * Instantiate our router.
     * The signature for the methods provided by our router is:
     *
     * @param string   $route
     * @param mixed    $action     : either a closure or a string indicating the controller@method
     * @param mixed    $parameters : either an array or a closure that will produce an array
     * @param mixed    $render     : optional - either a closure or a string indicating the view class.
     */
    $router = new Router();

Examples of routes include:

1. a route that uses a closure for the action:

        /**
         * Our home page.
         *
         * The most compact form of routing.
         * We have a route matching a path, and we supply a closure that 
         * performs the action and also renders the result.
         * Note that the Home controller is also acting as the view.
         */
        $router->get('/', function(){
            $class = new controllers\Home();
            $class->render();
        });

1. a route that uses a reference to a method on another class to provide the action:

        /**
         * Our SOAP client calling a method on the SOAP service.
         * E.G. http://localhost.soap/client/add?service=connector&x=10&y=5
         *
         * A more complex route, where we have a route matching a path, 
         * an action specified in the form of a controller and method to run,
         * and closures for the parameters and the renderer.
         * Note that we're passing our parameters here in the form of a closure,
         * as allows us to pass custom logic that the router can use, in this case
         * to determine the parameters for the action.
         * Note that we can also supply a view string, such as 'src\views\Client', rather than a closure.
         */
        $router->get('/client/add', 'src\controllers\Client@add',
            function() {
                $x = isset($_GET['x']) ? $_GET['x'] : '';
                $y = isset($_GET['y']) ? $_GET['y'] : '';
                return array($x, $y);
            },
            function($data) {
                $view = new views\Client($data);
                $view->render();
            }
        );

Closures are very useful here, as they allow us to enscapsulate the scope where they are defined, and pass logic to be invoked later by another class. The invoking class doesn't need to provide any scope to the closure, or know anything about it's logic, and can simply do something like the following:

    // We may have been passed a closure to determine our parameters.
    if (is_callable($parameters)) {
        $parameters = call_user_func($parameters);
    }

or even:

    // We're routing to a closure.
    $action();

## Reflection

Another recent PHP feature we make use of is [reflection](http://code.tutsplus.com/tutorials/reflection-in-php--net-31408), whereby we can interrogate our classes and methods and act accordingly.

For example, in our `Router.php` class, when supplied with an action of the form `src\controllers\Client@add` (which directs us to use a particular class method to provide an action to a route), we can instantiate our `Client` class as follows:

    // Controller actions are in the form 'ControllerClassName@methodName'.
    list($class, $method) = explode('@', $action);

    // Instantiate the controller class
    $instance = $this->build($class, $constructorArgs);

    ...

    /**
     * Instantiate a concrete instance of the given type.
     *
     * @param  string  $concrete
     * @param  array   $parameters
     * @return mixed
     */
    protected function build($concrete, $constructorArgs) {
        $reflector = new \ReflectionClass($concrete);
        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return new $concrete();
        }

        return $reflector->newInstanceArgs($constructorArgs);
    }

Here we're checking whether the class has a constructor, and calling it with any supplied arguments if it does, otherwise simply instantiating a new instance of the class.

A little later we can actually call the `add` method on the instantiated `Client` class:

    // call the method on the instance
    $result = $this->callAction($instance, $method, $parameters);

    ...

    protected function callAction($instance, $method, $parameters) {
        return call_user_func_array(array($instance, $method), $parameters);
    }

Similarly, we can render the data using either a supplied closure, or the name of a view class (e.g. `src\views\Client`):

    // Render any results
    if (is_callable($render)) {
        // We've got a closure
        call_user_func($render, $result);
    } elseif ($render) {
        // We've got the name of a view class which extends View.php.
        // Instantiate the view class.
        $view = $this->build($render, array());
        $view->render();
    } else {
        // We've not been provided with a view - simply spit out any results.
        echo $result;
    }

## Namespaces (PHP5.3+)

We make use of namespaces in our application to avoid conflicts with other thirdparty classes that may have the same name as one of our classes.

Composer's autoloader makes life easier for us here, and we can refer to relatively-qualified classes in the namespace, knowing that they'll be successfully resolved:

    // app.php

    namespace src;

    ...

    $class = new controllers\Connector();

Here we're referencing `controllers\Connector()` our `app.php` class which lives in the `src` namespace, and therefore has the fully-qualified namespace of `src/controllers/Connector`.

In `src/controllers/Connector.php`:

    // src/controllers/Connector.php

    namespace src\controllers;

We make life easier four ourselves (and for anyone maintaining / extending our code) by mapping our namespace directly onto our directory structure.

## Inheritence

PHP has long supported single class inheritence, and we make use of this:

    // src/controllers/Connector.php

    class Connector extends SoapServerBase {

Here we're extending from a parent class, `SoapServerBase`, and this allows us to create base/parent classes which enscapsulate functionality and make it available to child classes.

## Interfaces

Interfaces allow us to define an abstract contract that every class implenting the interface must adhere to:

    //src/interfaces/SoapServer.php

    namespace src\interfaces;

    /**
     * The interface for any SOAP server classes.
     */
    interface SoapServer {
     
        public function __construct();

        public function wsdl($class);

        public function serve($class);
    }

Any class implementing the interface must provide all methods (with the same signature) as defined in the interface:

    //src/controllers/SoapServerBase.php

    namespace src\controllers;
    use src\interfaces as interfaces;

    /**
     * Our SOAP server base class
     */
    class SoapServerBase implements interfaces\SoapServer {

In this project, we use interfaces to ensure that any SOAP clients or SOAP server classes will adhere to a consistent implementation.

Note that it as actually our `SoapServerBase` class which implements the interface, and this class is extended by our `SoapServer` class:

    // src/controllers/SoapServer.php

    namespace src\controllers;

    /**
     * Our SOAP service class
     *
     */
    class Connector extends SoapServerBase {

Thus, if we so desire, we can have multiple base classes, all implementing a common interface, but providing their own custom methods, and each base class can be inherited by a controller for an actual SOAP service.

## Traits (PHP5.4+)

One shortcoming of PHP has been the fact that it only supports single-class inheritence, however traits allow us to employ pseudo-multiple inheritence, amongst other purposes.

For example, consider the following interface:

    // src/interfaces/View.php

    namespace src\interfaces;

    /**
     * The interface for any view classes.
     */
    interface View {
     
        public function __construct();

        public function render();

        public function sanitise($string, $pattern);

    }

We employ this interface in our base `View.php` class:

    // src/views/View.php

    namespace src\views;
    use src\interfaces as interfaces;

    class View implements interfaces\View {

        private $template;
        private $data;

        /*
        ** We can specify a template, or fallback to a template that has 
        ** the same name as the child class.
        */
        public function __construct($data = '', $template = '') {
            if ($data) {
                $this->data = $data;
            }

            if ($template) {
                $this->template = $template;
            } else {
                $this->template = strtolower( self::getShortname(get_class($this)) );
            }
        }

        ...

        /*
        ** If no template exists, simply spit out our data.
        */
        public function render () {
            $template = TEMPLATE_PATH . '/' . $this->template;

            if (file_exists($template) && is_file($template)) {
                include($template);
            } else {
                echo $this->data;
            }
        }

        public function sanitise($string, $pattern) {

        }

    }

Note that here, we have to implement a `sanitise` method (as dictated by the interface), though we've chosen not to actually provide any functionality.

Elsewhere, in a child class that inherits from `src/views/View.php`: 

    // src/views/Client.php

    namespace src\views;

    trait Sanitise {
        public function sanitise($string, $pattern) {
            return preg_replace($pattern, '', $string);
        }

        public static $sanitiseInt = '/^[^1-9][^0-9]*$/';
    }

    class Client extends View {
        use Sanitise;

        public function __construct($data = '', $template = '') {
           parent::__construct($data, $template);
        }

    }

Our child `Client` class inherits from `View`, but also uses the `Sanitise` trait.

The `Sanitise` trait provides functionality that fulfills the `sanitise` method signature specified on the interface.
We can spin up a number of classes, each inheriting from the `View` base class, but where needed we can override the sanitise method using a trait.

We may have a number of these child classes that we want to use one form of the `Sanitise` trait, while we may have others that we want to use another form. In this situation, we effectively have a form of psuedo multiple inheritence.

Note that a class can use multiple traits, as well as employing multiple interfaces, using traits to share code between classes.

Note also that this is not true multiple inheritence, as traits are bound at compile time.

## Unit testing

We use PHPUnit for unit testing, and `test\unit\ConnectorTest.php` looks like:

    namespace src;

    /**
     * We rely on composer's autoloader.
     * To include classes in new directories, add the path to the 'autoload'
     * parameter in the composer.json and run 'composer dump-autoload'.
     */
    require __DIR__.'/../../vendor/autoload.php';

    /**
     * Pull in our configuration.
     */
    require_once __DIR__.'/../../src/config.php';

    class ConnectorTest extends \PHPUnit_Framework_TestCase {
        public function testConnector() {
            $class = new controllers\Connector();

            $this->assertEquals($class->add(2, 3), 5);
        }
    }

To run our tests either:

    grunt test.php

or

    phpunit test/unit

# How we got started

This section is historical, and is included to detail how we initially created this project.

## Installing the project skeleton using the Yeoman Webapp generator

We started from the [Yeoman Webapp generator](https://github.com/yeoman/generator-webapp), and modified it to include the desired componentry.

[Yeoman Webapp generator](https://github.com/yeoman/generator-webapp) provides us with:

* a [Gruntfile](http://gruntjs.com/) for specifying and running tasks to help us with things such as

    * checking the syntax of our CSS and JavaScript
    * concatenating and minifying the CSS and JavaScript
    * cache-busting our files
    * running our tests
    * web-serving our application

We installed Yeoman and inited the project skeleton as follows:

* At a command prompt, install yeoman, and the webapp generator:

        npm install yo

        npm install -g generator-webapp

        yo webapp

* Install the extra grunt components we need:

        npm install --save-dev grunt-php grunt-phpunit

* Add the following to our `grunt.initConfig`:

        php: {
            dist: {
                options: {
                    port: 8000,
                    base: '<%= config.app %>',
                    keepalive: true,
                    open: true
                }
            }
        },

        phpunit: {
            unit: {
                dir: 'test/unit'
            },
            options: {
                bin: 'vendor/bin/phpunit',
                //bootstrap: 'test/Bootstrap.php',
                colors: true,
                testdox: true
            }
        },

* Add the following tasks to the `Gruntfile.js`:

        grunt.registerTask('serve.php', ['php']);
        grunt.registerTask('test.php', ['phpunit:unit']);

* Add our composer file:

        composer init

* Add phpunit to our composer.json:

        "require-dev": {
            "phpunit/phpunit": "4.0.*"
        }

* Run the composer update to install our dependencies:

        composer update

* Enable our SOAP extension. Our PHP server uses the CLI php.ini, so ensure that the following is enabled:

        extension=php_soap.dll

* Create our `info.php` in `app`:

        &gt;?php 
            phpinfo();

* At our command prompt, runup our server:

        grunt serve.php

* Visit our `info.php` page:

        http://127.0.0.1:8000/info.php

</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>
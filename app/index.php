<?php

namespace src;

/**
 * Bootstrap our app.
 */
require_once __DIR__.'/../src/app.php';

/**
 * Instantiate our app.
 */
$app = new app();

/**
 * Run app.
 */
$app->run();